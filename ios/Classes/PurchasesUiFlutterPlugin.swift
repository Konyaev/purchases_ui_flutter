#if os(macOS)
import FlutterMacOS
#else
import Flutter
#endif
import PurchasesHybridCommon
import Foundation
import UIKit
import RevenueCat

@available(iOS 15.0, *)
public class PurchasesUiFlutterPlugin: NSObject, FlutterPlugin {

    private static let BAD_ARGS_ERROR_CODE = "BAD_ARGS"

    public static func register(with registrar: FlutterPluginRegistrar) {

        #if os(macOS)
        let messenger = registrar.messenger
        #else
        let messenger = registrar.messenger()
        #endif
        let channel = FlutterMethodChannel(name: "purchases_ui_flutter", binaryMessenger: messenger)
        let instance = PurchasesUiFlutterPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    private var _paywallProxy: Any?

    #if os(iOS)
    @available(iOS 15.0, *)
    private var paywallProxy: PaywallProxy {
        get {
            // swiftlint:disable:next force_cast
            return self._paywallProxy as! PaywallProxy
        }

        set {
            self._paywallProxy = newValue
        }
    }
    #endif

    private var customPaywall: CustomPaywall

    override init() {
        #if os(iOS)
        if #available(iOS 15.0, *) {
            self._paywallProxy = PaywallProxy()
        }
        #endif
        customPaywall = CustomPaywall()
        super.init()
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "presentPaywall":
            self.presentPaywall(result, requiredEntitlementIdentifier: nil, closeButton: nil)
        case "presentPaywallIfNeeded":
            guard let args = call.arguments as? Dictionary<String, Any> else {
                result(FlutterError(code: PurchasesUiFlutterPlugin.BAD_ARGS_ERROR_CODE,
                                    message: "Invalid arguments type",
                                    details: nil))
                return
            }
            guard let requiredEntitlementIdentifier = args["requiredEntitlementIdentifier"] as? String? else {
                result(FlutterError(code: PurchasesUiFlutterPlugin.BAD_ARGS_ERROR_CODE,
                                    message: "Missing requiredEntitlementIdentifier in presentPaywallIfNeeded",
                                    details: nil))
                return
            }

            guard let closeButton = args["closeButton"] as? Bool else {
                result(FlutterError(code: PurchasesUiFlutterPlugin.BAD_ARGS_ERROR_CODE, message: "Missing closeButton in presentPaywallIfNeeded", details: nil))
                return
            }

            guard let blockFlutterUI = args["blockFlutterUI"] as? Bool else {
                result(FlutterError(code: PurchasesUiFlutterPlugin.BAD_ARGS_ERROR_CODE, message: "Missing blockFlutterUI in presentPaywallIfNeeded", details: nil))
                return
            }

            Purchases.shared.getOfferings { (offerings, error) in
                if let error = error {
                    print("Error fetching offerings: \(error)")
                    result(FlutterError(code: "OFFERINGS_ERROR", message: "Error fetching offerings: \(error)", details: nil))
                    return
                }

                guard let offerings = offerings else {
                    print("No offerings available")
                    result(FlutterError(code: "NO_OFFERINGS", message: "No offerings available", details: nil))
                    return
                }

                let offeringID = args["offering"] as? String
                let specificOffering = offerings.offering(identifier: offeringID) ?? offerings.current

                self.customPaywall.customPaywall(requiredEntitlementIdentifier: requiredEntitlementIdentifier, displayCloseButton: closeButton, blockFlutterUI: blockFlutterUI, offering: specificOffering) { paywallResultString in
                    result(paywallResultString)
                }
            }

            // self.presentPaywall(result, requiredEntitlementIdentifier: requiredEntitlementIdentifier, closeButton: closeButton)
        default:
            result(FlutterMethodNotImplemented)
        }
    }

    private func presentPaywall(_ result: @escaping FlutterResult, requiredEntitlementIdentifier: String?, closeButton: Bool?) {
        #if os(iOS)
        if #available(iOS 15.0, *) {
            if let requiredEntitlementIdentifier {
                self.paywallProxy.presentPaywallIfNeeded(requiredEntitlementIdentifier: requiredEntitlementIdentifier, displayCloseButton: closeButton!) { paywallResultString in
                    result(paywallResultString)
                }
            } else {
                self.paywallProxy.presentPaywall { paywallResultString in
                    result(paywallResultString)
                }
            }
        } else {
            NSLog("Presenting paywall requires iOS 15+")
        }
        #else
        NSLog("Presenting paywall requires iOS")
        #endif
    }

}


@available(iOS 15.0, *)
@objcMembers public class CustomPaywall: NSObject {
    public weak var delegate: PaywallViewControllerDelegate?
    private var resultByVC: [PaywallViewController: (paywallResultHandler: (String) -> Void,
                                                     result: PaywallResult)] = [:]

    public func customPaywall(requiredEntitlementIdentifier: String?,
                            displayCloseButton: Bool?,
                            blockFlutterUI: Bool,
                            offering: Offering?,
                            paywallResultHandler: ((String) -> Void)? = nil) {
        guard let scene = UIApplication.shared.connectedScenes.first(where: { $0.activationState == .foregroundActive }),
            let windowScene = scene as? UIWindowScene,
            let rootController = windowScene.windows.first?.rootViewController else {
            return
        }

        _ = Task { @MainActor in
            do {
                let customerInfo = try await Purchases.shared.customerInfo()
                let shouldDisplay = requiredEntitlementIdentifier.map { 
                    !customerInfo.entitlements.active.keys.contains($0) 
                } ?? true

                if shouldDisplay {
                    let controller: PaywallViewController
                    if let displayCloseButton = displayCloseButton {
                        controller = PaywallViewController(offering: offering, displayCloseButton: displayCloseButton)
                    } else {
                        controller = PaywallViewController()
                    }

                    controller.delegate = self
                    controller.modalPresentationStyle = blockFlutterUI ? .fullScreen : .overFullScreen
                    controller.modalTransitionStyle = .coverVertical
                    controller.view.isExclusiveTouch = true
                    if let paywallResultHandler {
                        self.resultByVC[controller] = (paywallResultHandler, .cancelled)
                    }
                    rootController.present(controller, animated: true)
                } else {
                    paywallResultHandler?(PaywallResult.notPresented.name)
                }
            } catch {
                NSLog("Failed presenting paywall: \(error)")
            }
        }
    }
}

internal enum PaywallResult {
    case notPresented
    case purchased
    case restored
    case cancelled

    var name: String {
        switch self {
        case .notPresented: return "NOT_PRESENTED"
        case .purchased: return "PURCHASED"
        case .restored: return "RESTORED"
        case .cancelled: return "CANCELLED"
        }
    }
}

@available(iOS 15.0, *)
extension CustomPaywall: PaywallViewControllerDelegate {

    public func paywallViewController(_ controller: PaywallViewController,
                                      didFinishPurchasingWith customerInfo: CustomerInfo) {
        self.resultByVC[controller]?.1 = .purchased
        self.delegate?.paywallViewController?(controller, didFinishPurchasingWith: customerInfo)
    }

    public func paywallViewController(_ controller: PaywallViewController,
                                      didFinishPurchasingWith customerInfo: CustomerInfo,
                                      transaction: StoreTransaction?) {
        self.delegate?.paywallViewController?(controller, didFinishPurchasingWith: customerInfo, transaction: transaction)
    }

    public func paywallViewController(_ controller: PaywallViewController,
                                      didFinishRestoringWith customerInfo: CustomerInfo) {
        self.resultByVC[controller]?.1 = .restored
        self.delegate?.paywallViewController?(controller, didFinishRestoringWith: customerInfo)
    }

    public func paywallViewControllerWasDismissed(_ controller: PaywallViewController) {
        self.delegate?.paywallViewControllerWasDismissed?(controller)
        guard let (paywallResultHandler, result) = self.resultByVC.removeValue(forKey: controller) else { return }
        paywallResultHandler(result.name)
    }

    public func paywallViewController(_ controller: PaywallViewController, 
                                      didChangeSizeTo size: CGSize) {
        self.delegate?.paywallViewController?(controller, didChangeSizeTo: size)
    }

}